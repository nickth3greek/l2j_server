/*
 * Copyright (C) 2004-2016 L2J Server
 * 
 * This file is part of L2J Server.
 * 
 * L2J Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2J Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.idfactory;

import java.util.BitSet;
import java.util.concurrent.atomic.AtomicInteger;

import com.l2jserver.gameserver.ThreadPoolManager;
import com.l2jserver.util.PrimeFinder;

/**
 * This class ..
 * @version $Revision: 1.2 $ $Date: 2004/06/27 08:12:59 $
 */
public class BitSetIDFactory extends IdFactory
{
	private BitSet _freeIds;
	private AtomicInteger _freeIdCount;
	private AtomicInteger _lastFreeId;
	
	protected class BitSetCapacityCheck implements Runnable
	{
		@Override
		public void run()
		{
			synchronized (BitSetIDFactory.this)
			{
				if (reachingBitSetCapacity())
				{
					increaseBitSetCapacity();
				}
			}
		}
	}
	
	protected BitSetIDFactory()
	{
		super();
		
		synchronized (BitSetIDFactory.class)
		{
			ThreadPoolManager.getInstance().scheduleGeneralAtFixedRate(new BitSetCapacityCheck(), 30000, 30000);
			initialize();
		}
		_log.info(getClass().getSimpleName() + ": " + _freeIds.size() + " id's available.");
	}
	
	private void initialize()
	{
		try
		{
			_freeIds = new BitSet(PrimeFinder.nextPrime(100000));
			_freeIds.clear();
			_freeIdCount = new AtomicInteger(FREE_OBJECT_ID_SIZE + 1);
			
			for (int usedObjectId : extractUsedObjectIDTable())
			{
				int objectID = usedObjectId - FIRST_OID;
				if (objectID < 0)
				{
					_log.warning(getClass().getSimpleName() + ": Object ID " + usedObjectId + " in DB is less than minimum ID of " + FIRST_OID);
					continue;
				}
				_freeIds.set(usedObjectId - FIRST_OID);
				_freeIdCount.decrementAndGet();
			}
			
			_lastFreeId = new AtomicInteger(_freeIds.nextClearBit(0));
			_initialized = true;
		}
		catch (Exception e)
		{
			_initialized = false;
			_log.severe(getClass().getSimpleName() + ": Could not be initialized properly: " + e.getMessage());
		}
	}
	
	@Override
	public synchronized void releaseId(int objectID)
	{
		// TODO check release of non existing object
		if ((objectID - FIRST_OID) > -1)
		{
			_freeIds.clear(objectID - FIRST_OID);
			_freeIdCount.incrementAndGet();
		}
		else
		{
			_log.warning(getClass().getSimpleName() + ": Release objectID " + objectID + " failed (< " + FIRST_OID + ")");
		}
	}
	
	@Override
	public synchronized int getNextId()
	{
		int nextFree = _freeIds.nextClearBit(_lastFreeId.get());
		
		// Search for released indexes/OIDs
		if (((nextFree + FIRST_OID) > LAST_OID) || ((nextFree + FIRST_OID) < 0))
		{
			nextFree = _freeIds.nextClearBit(0);
		}
		
		if (((nextFree + FIRST_OID) > LAST_OID) || ((nextFree + FIRST_OID) < 0))
		{
			throw new NullPointerException("Ran out of valid Id's.");
		}
		
		_lastFreeId.set(nextFree);
		_freeIds.set(nextFree);
		_freeIdCount.decrementAndGet();
		
		return nextFree + FIRST_OID;
	}
	
	@Override
	public synchronized int size()
	{
		return _freeIdCount.get();
	}
	
	/**
	 * @return
	 */
	protected synchronized int usedIdCount()
	{
		return (FREE_OBJECT_ID_SIZE + 1) - size();
	}
	
	/**
	 * @return
	 */
	protected synchronized boolean reachingBitSetCapacity()
	{
		final int newCapacity = (int) (((long) usedIdCount() * 11) / 10);
		return (newCapacity < 0 ? Integer.MAX_VALUE : PrimeFinder.nextPrime(newCapacity)) > _freeIds.size();
	}
	
	protected synchronized void increaseBitSetCapacity()
	{
		final int newCapacity = (int) (((long) usedIdCount() * 11) / 10);
		final BitSet newBitSet = new BitSet(newCapacity < 0 ? Integer.MAX_VALUE : PrimeFinder.nextPrime(newCapacity));
		newBitSet.or(_freeIds);
		_freeIds = newBitSet;
	}
}
